(function () {
    'use strict';
    angular.module('myApp.common')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['AuthenticationService'];
    function LoginController(AuthenticationService) {
        var vm = this;
        vm.login = login;

        function login() {
            AuthenticationService.login({ email: vm.email_username,username: vm.email_username, password: vm.password });
            vm.pageClass = 'fadeZoom';
        }
    }
})();