/**
 * Created by jvandewync on 8/10/2016.
 */

(function () {
    'use strict';
    angular.module('myApp.common')
        .controller('SignUpController', SignUpController);

    SignUpController.$inject = ['AuthenticationService'];
    function SignUpController(AuthenticationService) {
        var vm = this;
        vm.signup = signup;

        function signup() {
            AuthenticationService.signup(vm.user);
            vm.pageClass = 'fadeZoom';
        }
    }
})();