/**
 * Created by jvandewync on 8/10/2016.
 */

(function() {
    'use strict';
    angular.module('myApp.common')
        .controller('NavbarController',NavbarController);

    NavbarController.$inject = ['AuthenticationService'];
    function NavbarController(AuthenticationService) {
        var vm = this;
        vm.logout = logout;

        function logout() {
            AuthenticationService.logout();
        }
    }

})();