/**
 * Created by jvandewync on 8/10/2016.
 */
(function () {
    'use strict';

    angular.module('myApp.common',
        ['ngRoute',
            'ngMessages',
            'ngAnimate', 'mgcrea.ngStrap'])
        .config(httpInterceptorConfig)
        .config(routerConfig);

    function httpInterceptorConfig($httpProvider) {
        $httpProvider.interceptors.push(function ($rootScope, $q, $window, $location) {
            return {
                request: function (config) {
                    if ($window.localStorage.token) {
                        config.headers.Authorization = 'Bearer ' + $window.localStorage.token;
                    }
                    return config;
                },
                responseError: function (response) {
                    if (response.status === 401 || response.status === 403) {
                        $location.path('/auth/login');
                    }
                    if (response.status === 404) {
                        $location.path('/pageNotFound');
                    }
                    return $q.reject(response);
                }
            }
        });
    }

    function routerConfig($routeProvider, $locationProvider) {
        $routeProvider
            .when('/auth/signup/', {
                templateUrl: 'register.html',
                controller: 'SignUpController',
                controllerAs: 'vm'
            })
            .when('/auth/login/', {
                templateUrl: 'login.html',
                controller: 'LoginController',
                controllerAs: 'vm'
            })
            .when('/pageNotFound', {
                templateUrl: 'pageNotFound.html'
            })
    }
})();
