var myApp = angular.module('myApp.common');


myApp.factory('AuthenticationService', ['$http', '$location', '$rootScope', '$alert', '$window', function ($http, $location, $rootScope, $alert, $window) {

    var token = $window.localStorage.token;
    if (token && token!= "undefined") {
        var payload = JSON.parse($window.atob(token.split('.')[1]));
        $rootScope.currentUser = payload.auth.local.username;
    }
    return {
        login: function (user) {
            return $http.post('/auth/login', user)
                .then((data) => {
                    $window.localStorage.token = data.data.token;
                    var payload = JSON.parse($window.atob(data.data.token.split('.')[1]));
                    $rootScope.currentUser = payload.auth.local.username;
                    $location.path('/secure/dashboard');
                    $alert({
                        title: 'Cheers!',
                        content: 'You have successfully logged in.',
                        animation: 'fadeZoomFadeDown',
                        type: 'material',
                        duration: 3
                    });
            }
			, (response) => {
                    delete $window.localStorage.token;
                    $alert({
                        title: 'Error!',
                        content: response.statusText,
                        animation: 'fadeZoomFadeDown',
                        type: 'material',
                        duration: 3
                    });
                });
        },
        signup: function (user) {
            return $http.post('/auth/signup', user)
                .then( (data) =>{

                    $window.localStorage.token = data.data.token;
                    var payload = JSON.parse($window.atob(data.data.token.split('.')[1]));
                    $rootScope.currentUser = payload.auth.local.username;
                    $location.path('/secure/dashboard');

                    $alert({
                        title: 'Congratulations!',
                        content: 'Your account has been created.',
                        animation: 'fadeZoomFadeDown',
                        type: 'material',
                        duration: 3
                    });
                }
                ,(response) => {
                    //$location.path('/auth/signup');
                    console.log(response.message);
                    $alert({
                        title: 'Error!',
                        content: response.statusText,
                        animation: 'fadeZoomFadeDown',
                        type: 'material',
                        duration: 3
                    });
                });
        },
        logout: function () {
            delete $window.localStorage.token;
            $rootScope.currentUser = null;
            $http.get('/auth/logout')
                .then(function () {
                $location.path('/');
                $alert({
                    content: 'You have been logged out.',
                    animation: 'fadeZoomFadeDown',
                    type: 'material',
                    duration: 3
                });
            });
        }
    }
}]);
