/**
 * Created by jvandewync on 8/10/2016.
 */

(function () {
    angular.module('myApp.common')
        .directive('passwordStrength', passwordStrength)

    function passwordStrength() {
        var directive = {
            restrict: 'AE',
            require: 'ngModel',
            scope: false,
            "template": ['<div class="password-strengh"><span class="password-strengh-icons glyphicon" ng-class="pwdValidLength" ng-style="{color: pwdValidLengthColor}" ></span>8 Characters Long<br>',
                '<span class="password-strengh-icons glyphicon" ng-class="pwdHasLetter" ng-style="{color: pwdHasLetterColor}" ></span>One Uppercase Letter<br>',
                '<span class="password-strengh-icons glyphicon" ng-class="pwdHasNumber" ng-style="{color: pwdHasNumberColor}" ></span>One Number</div>'].join(''),
            link: changeCSS
        };

        return directive;

        function changeCSS(scope, element, attrs, ngModel) {
            var indicator = element.children();
            element.after(indicator);

            initCSS();
            element.bind('keyup', function () {
                var password = scope.vm.user.password;

                if (password) {
                    var testCorrectLength = hasCorrectLength(password);
                    var testContainsDigit = containsDigit(password);
                    var testContainsUpperCaseLetter = containsUpperCaseLetter(password);
                    ngModel.$setValidity("password", testCorrectLength && testContainsDigit && testContainsUpperCaseLetter);
                }
                else {
                    initCSS();
                }

                scope.$apply();
            });

            function initCSS() {
                scope.pwdValidLength = "glyphicon-remove";
                scope.pwdValidLengthColor = '#FF0004';

                scope.pwdHasLetter = "glyphicon-remove";
                scope.pwdHasLetterColor = '#FF0004';

                scope.pwdHasNumber = "glyphicon-remove";
                scope.pwdHasNumberColor = '#FF0004';
            }

            function hasCorrectLength(password) {
                if (password.length >= 8) {
                    scope.pwdValidLength = "glyphicon-ok";
                    scope.pwdValidLengthColor = '#009900';
                    return true;
                }
                else {
                    scope.pwdValidLength = "glyphicon-remove";
                    scope.pwdValidLengthColor = '#FF0004';
                    return false;
                }
            }

            function containsUpperCaseLetter(password) {
                if (/[A-Z]/.test(password)) {
                    scope.pwdHasLetter = "glyphicon-ok";
                    scope.pwdHasLetterColor = '#009900';
                    return true;
                }
                else {
                    scope.pwdHasLetter = "glyphicon-remove";
                    scope.pwdHasLetterColor = '#FF0004';
                    return false;
                }
            }

            function containsDigit(password) {
                if (/\d/.test(password)) {
                    scope.pwdHasNumber = "glyphicon-ok";
                    scope.pwdHasNumberColor = '#009900';
                    return true;
                }
                else {
                    scope.pwdHasNumber = "glyphicon-remove";
                    scope.pwdHasNumberColor = '#FF0004';
                    return false;
                }
            }
        }
    }

})();