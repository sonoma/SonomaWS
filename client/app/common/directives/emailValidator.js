/**
 * Created by jvandewync on 8/10/2016.
 */

(function () {
    angular.module('myApp.common')
        .directive('emailValidator', emailValidator);

    function emailValidator() {
        var directive = {
            restrict: 'AE',
            require: 'ngModel',
            link: link
        };

        return directive;

        function link(scope, element, attrs, ngModel) {
            // only apply the validator if ngModel is present and Angular has added the email validator
            if (ngModel && ngModel.$validators.email) {

                // this will overwrite the default Angular email validator
                ngModel.$validators.email = function (modelValue) {
                    return ngModel.$isEmpty(modelValue) || validator.isEmail(modelValue);
                };
            }

        }
    }
})();