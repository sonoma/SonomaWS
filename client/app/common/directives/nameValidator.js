/**
 * Created by jvandewync on 8/28/2016.
 */

(function () {
    angular.module('myApp.common')
        .directive('nameValidator', nameValidator);

    function nameValidator() {
        var directive = {
            restrict: 'AE',
            require: 'ngModel',
            link: link
        };

        return directive;

        function link(scope, element, attrs, ngModel) {
            if (ngModel && ngModel.$validators.name) {

                // this will overwrite the default Angular email validator
                ngModel.$validators.name = function (modelValue) {
                    return ngModel.$isEmpty(modelValue) || validator.isAlpha(modelValue);
                };
            }

        }
    }
})();