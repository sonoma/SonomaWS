/**
 * Created by jvandewync on 8/28/2016.
 */

(function () {
    angular.module('myApp.common')
        .directive('usernameValidator', usernameValidator);

    function usernameValidator() {
        var directive = {
            restrict: 'AE',
            require: 'ngModel',
            link: link
        };

        return directive;

        function link(scope, element, attrs, ngModel) {
            if (ngModel && ngModel.$validators.username) {

                // this will overwrite the default Angular email validator
                ngModel.$validators.username = function (modelValue) {
                    return ngModel.$isEmpty(modelValue) || validator.isAlphanumeric(modelValue);
                };
            }
        }
    }
})();