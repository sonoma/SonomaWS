/**
 * Created by jvandewync on 8/21/2016.
 */

var myApp = angular.module('myApp.dashboard');

myApp.directive('displayEChart', function () {
    return {
        scope: true,
        //template : '<div style="height:400px;width:640px;"></div>',
        link: function ($scope, element, attrs, ctrl) {

            function initErrorMarkLine() {
                console.log($scope.selectedChart.errorMarkLine)
                return {
                    animation: false,
                    label: {
                        normal: {
                            formatter: $scope.selectedChart.categoryname || 'Error',
                            textStyle: {
                                align: 'center'
                            }
                        }
                    },
                    tooltip: {
                        formatter: $scope.selectedChart.timeAxis[$scope.selectedChart.errorMarkLine]
                    },
                    data: [{
                        xAxis: $scope.selectedChart.errorMarkLine
                    }]
                }
            }

            function initUiOptions() {
                return {
                    toolbox: {
                        show: true,
                        showTitle: true,
                        feature: {
                            dataView: {
                                show: true,
                                title: 'Data View',
                                lang: ['Data View', 'Close', 'Refresh']
                            },
                            restore: {
                                show: true,
                                title: 'Restore'
                            },
                            saveAsImage: {
                                show: true,
                                title: 'Save as Image',
                                lang: ['Click to Save']
                            }
                        }
                    },
                    dataZoom: [{
                        type: 'inside',
                        start: 12.5, // 0.5s * 100% / 4s
                        end: 37.5
                    }, {
                        start: 12.5,
                        end: 37.5,
                        handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                        handleSize: '80%',
                        handleStyle: {
                            color: '#fff',
                            shadowBlur: 3,
                            shadowColor: 'rgba(0, 0, 0, 0.6)',
                            shadowOffsetX: 2,
                            shadowOffsetY: 2
                        }
                    }
                    ],
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: ['Voltage', 'Current']
                    },
                    xAxis: [
                        {
                            type: 'category',
                            boundaryGap: false,
                            data: $scope.selectedChart.timeAxis
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            boundaryGap: [0, '100%'],
                            axisLabel: {
                                formatter: function (value, index) {
                                    return value.toFixed(6);
                                }
                            }
                        }
                    ],
                    series: [
                        {
                            name: 'Voltage',
                            type: 'line',
                            smooth: true,
                            symbol: 'none',
                            sampling: 'average',
                            data: $scope.selectedChart.voltagedata
                        },
                        {
                            name: 'Current',
                            type: 'line',
                            smooth: true,
                            symbol: 'none',
                            sampling: 'average',
                            data: $scope.selectedChart.currentdata
                        }]
                }
            }

            var myChart = echarts.init(element[0]);

            if (attrs.uiOptions) {
                attrs.$observe('uiOptions', function () {
                    var options = $scope.$eval(attrs.uiOptions);
                    if (angular.isObject(options)) {
                        myChart.setOption(options);
                    }
                }, true);
            }

            $scope.$watch('selectedChart', function (newValue, oldValue) {
                if (null != newValue) {
                    var baseOptions = initUiOptions();
                    var errorMarkLine = initErrorMarkLine();

                    baseOptions.series[0].markLine = errorMarkLine;
                    myChart.setOption(baseOptions);

                    myChart.on('legendselectchanged', function (params) {
                        if (params.selected) {

                            var currentOptions = myChart.getOption();

                            console.log(params.selected);

                            var index = 0;
                            var first = true;
                            Object.keys(params.selected).forEach(function (key) {
                                if (params.selected[key] && first) {
                                    currentOptions.series[index].markLine = errorMarkLine;
                                    first = false;
                                }
                                else {
                                    currentOptions.series[index].markLine = {};
                                }
                                index++;
                            });
                            myChart.setOption(currentOptions, true, false);
                        }
                    });
                }
            });

        }
    };
});