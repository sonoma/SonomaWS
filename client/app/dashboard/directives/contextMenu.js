(function () {
    angular.module('myApp.dashboard')
        .directive('contextMenu', contextMenu);

    contextMenu.$inject = ['$rootScope'];
    function contextMenu($rootScope) {
        var directive = {
            restrict: 'AE',
            link: link,
            controller : 'ContextMenuController'
        };

        return directive;

        function link(scope, element, attrs, controller) {

            element.on('contextmenu', function (event) {
                $rootScope.$apply(function() {
                    event.preventDefault();
                    event.stopPropagation();

                    var id = element[0].attributes["value"].value;
                    $rootScope.$broadcast('download', id);

                    // Hide current menu if any is showing
                    if ($rootScope.activeContextMenuController) {
                        $rootScope.activeContextMenuController.hide();
                    }
                    // Show new menu
                    var items = scope.$eval(attrs.contextMenu);
                    controller.show(event.pageX, event.pageY, items);
                    $rootScope.activeContextMenuController = controller;
                });
            });

        }
    }
})();