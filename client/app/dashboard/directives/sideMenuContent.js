/**
 * Created by jvandewync on 8/23/2016.
 */

/**
 * Created by jvandewync on 8/20/2016.
 */

var myApp = angular.module('myApp.dashboard');

myApp.directive('sideMenuContent', ['$compile', function ($compile) {
    return {
        restrict : 'AE',
        scope: false,
        transclude : true,
        link: function (scope, element, attrs, ctrl) {

            scope.$watch('errorOccurrences', function(newValue, oldValue) {
                if (null != newValue){
                    console.log(scope.errorOccurrences);
                }
            }, true);
        }
    }
}]);