
var myApp = angular.module('myApp.dashboard');

myApp.controller('displayEChartCtrl',['$scope','chartGetter', function ($scope,chartGetter) {


    $scope.$on('graph', function (event, selectedChartId) {
        var temp = chartGetter.grabFromDBbyID(selectedChartId);
        temp.then( (chart)=>{
            console.log(chart.data.selectedChart);
            $scope.selectedChart = chart.data.selectedChart;
        },(status)=>{
            console.log(status);
        });
    });

}]);
