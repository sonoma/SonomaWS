/**
 * Created by jvandewync on 8/30/2016.
 */

/**
 * Created by jvandewync on 8/10/2016.
 */

(function () {
    'use strict';
    angular.module('myApp.dashboard')
        .controller('ContextMenuController', ContextMenuController);

    ContextMenuController.$inject = ['$scope', '$document', '$element', '$window'];
    function ContextMenuController($scope, $document, $element, $window) {
        var vm = this;

        // Contains menus and submenus created by this context menu
        var activeMenus = [];
        var isVisible = false;
        // Create nested menus at the left side
        var nestedAtLeft = false;


        // Keyboard input
        const KEYCODE_LEFT = 37;
        const KEYCODE_RIGHT = 39;
        const KEYCODE_UP = 38;
        const KEYCODE_DOWN = 40;
        const KEYCODE_CONFIRM = 13;
        var selectedMenuIndex = 0;
        var selectedButtonIndex = 0;

        /**
         * Show this context menu
         * @param {Number} x - horizontal position
         * @param {Number} y - vertical position
         * @param {Array} items - array of menu elements
         */
        vm.show = function (x, y, items) {
            isVisible = true;
            nestedAtLeft = false;

            var menu = vm.addMenu(x, y, items);
            y = menu.element[0].offsetTop;

            // Check for leaving the screen horizontally
            setMenuPosition(menu.element, 0, 0);
            var menuWidth = menu.element[0].clientWidth;
            setMenuPosition(menu.element, x, y);

            if (x + menuWidth > angular.element($window)[0].clientWidth) {
                x -= menuWidth;
                setMenuPosition(menu.element, x, y);
                nestedAtLeft = true;
            }
        };

        vm.hide = function () {
            if (!isVisible) {
                return;
            }
            isVisible = false;
            angular.forEach(activeMenus, function (menu) {
                menu.element.remove();
            });
            activeMenus = [];
        };

        function getMenuAtDepth(depth) {
            var result = null;
            angular.forEach(activeMenus, function (menu) {
                if (menu.depth == depth) {
                    result = menu;
                }
            });
            return result;
        }

        // Moves menu at given position on the screen
        function setMenuPosition(menuElement, x, y) {
            menuElement.css({
                position: 'absolute',
                left: x + 'px',
                top: y + 'px',
                // Menu should aways be on the top of all elements
                'z-index': 99999
            });
        }

        vm.addMenu = function (x, y, items, depth, parentButtonElement) {
            depth = typeof depth !== 'undefined' ? depth : 0;
            parentButtonElement = typeof parentButtonElement !== 'undefined' ? parentButtonElement : null;

            if (typeof(items) !== 'object') {
                return null;
            }

            var div = angular.element('<div>');
            div.addClass('list-group context-menu');
            div.attr({role: 'group'});

            var menu = {
                element: div,
                buttons: [],
                depth: depth, // level of nesting
                parentButtonElement: parentButtonElement
            };

            angular.forEach(items, function (item) {
                if (typeof(item) !== 'object') {
                    return;
                }
                var button = angular.element('<a>');
                button.addClass('list-group-item context-menu-item');
                button.append(item.text + '&nbsp;');
                button.attr({href: '#'});

                if (item.submenu) {
                    var span = angular.element('<span>');
                    span.addClass('glyphicon glyphicon-triangle-right pull-right');
                    button.append(span);
                }
                div.append(button);

                var isEnabled = isButtonItemEnabled(item);

                if (isEnabled) {
                    button.on('click', function (event) {
                        handleItemClick(item);
                    });
                    button.on('mouseover', function (event) {
                        handleItemMouseOver(menu, button, item);
                    });
                } else {
                    button.addClass('disabled');
                }

                menu.buttons.push({element: button, item: item});
            });

            // Add menu to the DOM
            angular.element($document).find('body').append(div);
            activeMenus.push(menu);

            // Check for leaving the screen vertically
            setMenuPosition(div, 0, 0);
            var menuHeight = div[0].offsetHeight;
            y = Math.min(y, angular.element($window)[0].innerHeight - menuHeight + $window.pageYOffset);
            setMenuPosition(div, x, y);
            return menu;

        };

        function isButtonItemEnabled(item) {
            if (typeof(item.enabled) === 'boolean') {
                return item.enabled;
            } else if (typeof(item.enabled) !== 'string') {
                return true;
            }
            return $scope.$eval(item.enabled);
        }

        function handleItemClick(item) {
            if (!isButtonItemEnabled(item)) {
                return;
            }
            switch (typeof(item.click)) {
                case 'function':
                    item.click();
                    break;
                case 'string':
                    $scope.$eval(item.click);
                    break;
                default:
                    return;
            }
            vm.hide();
        }

        function handleItemMouseOver(menu, button, item, fromKeyboard) {
            fromKeyboard = typeof fromKeyboard !== 'undefined' ? fromKeyboard : false;

            // Remove keyboard selection
            if (activeMenus[selectedMenuIndex] && !fromKeyboard) {
                var selectedMenu = activeMenus[selectedMenuIndex];
                var selectedButton = selectedMenu.buttons[selectedButtonIndex];
                selectedButton.element.removeClass('active');
                selectedButtonIndex = 0;
                selectedMenuIndex = 0;
            }
            // Hide all nested menus at higher levels
            var menusToRemove = [];
            angular.forEach(activeMenus, function (m) {
                if (m.depth > menu.depth) {
                    menusToRemove.push(m);
                }
            });

            angular.forEach(menusToRemove, function (m) {
                vm.removeMenu(m);
            });

            // Show the nested menu
            if (item.submenu) {
                var position = angular.element(button).offset();
                var menuWidth = menu.element.width();
                var x = position.left + menuWidth;
                var y = position.top;

                var nestedMenu = vm.addMenu(x, y, item.submenu, menu.depth + 1, button);
                y = nestedMenu.element.offset().top;

                // Check menu for leaving the screen
                // Move menu to (0,0), to restore the maximal size,
                // otherwise menu width could shrink at the edge
                // of the page
                setMenuPosition(nestedMenu.element, 0, 0);
                var nestedMenuWidth = nestedMenu.element.width();
                setMenuPosition(nestedMenu.element, x, y);

                if (nestedAtLeft || x + nestedMenuWidth > angular.element($window).width()) {
                    x = position.left - nestedMenuWidth;
                    setMenuPosition(nestedMenu.element, x, y);
                    // All other menus should be created at the left side from now
                    nestedAtLeft = true;
                }
            }
        }

        vm.removeMenu = function (menu) {
            if (!isVisible) {
                return;
            }
            var index = activeMenus.indexOf(menu);
            if (index < 0) {
                return;
            }
            activeMenus.splice(index, 1);
            menu.element.remove();
        };

        $document.on('mousedown', function (event) {
            if (!isVisible) {
                return;
            }
            // Don't hide if target is a menu or a menu button
            if (angular.element(event.target).hasClass('context-menu') ||
                angular.element(event.target).hasClass('context-menu-item')) {
                return;
            }
            // Hide otherwise
            vm.hide();
        });

        // Menu nvigation using keyboard

        $document.on('mousedown', function (event) {
            if (!isVisible) {
                return;
            }
            var selectedMenu = activeMenus[selectedMenuIndex];
            if (!selectedMenu) {
                selectedMenuIndex = 0;
                selectedButtonIndex = 0;
                return;
            }
            var selectedButton = selectedMenu.buttons[selectedButtonIndex];
            var previousSelectedButtonIndex = selectedButtonIndex;

            switch (event.keyCode) {
                // Choose button depending on menu direction
                case nestedAtLeft ? KEYCODE_RIGHT : KEYCODE_LEFT:
                    var parentMenu = getMenuAtDepth(selectedMenu.depth - 1);
                    if (parentMenu) {
                        var buttonIndex = 0;
                        // Get button that opens this submenu
                        angular.forEach(parentMenu.buttons, function (button, index) {
                            if (button.element == selectedMenu.parentButtonElement) {
                                buttonIndex = index;
                                return;
                            }
                        });
                        // Go to the parent menu
                        selectedMenuIndex = activeMenus.indexOf(parentMenu);
                        selectedMenu = activeMenus[selectedMenuIndex];
                        previousSelectedButtonIndex = -1;
                        selectedButtonIndex = buttonIndex;
                    }
                    break;
                // Choose button depending on menu direction
                case nestedAtLeft ? KEYCODE_LEFT : KEYCODE_RIGHT:
                    if (selectedButton.item.submenu) {
                        var nestedMenu = getMenuAtDepth(selectedMenu.depth + 1);
                        if (nestedMenu) {
                            // Go to the nested menu
                            selectedMenuIndex = activeMenus.indexOf(nestedMenu);
                            selectedMenu = activeMenus[selectedMenuIndex];
                            previousSelectedButtonIndex = -1;
                            selectedButtonIndex = 0;
                        }
                    }
                    break;
                case KEYCODE_UP:
                    selectedButtonIndex--;
                    if (selectedButtonIndex < 0) {
                        selectedButtonIndex = selectedMenu.buttons.length - 1;
                    }
                    break;
                case KEYCODE_DOWN:
                    selectedButtonIndex++;
                    if (selectedButtonIndex >= selectedMenu.buttons.length) {
                        selectedButtonIndex = 0;
                    }
                    break;
                case KEYCODE_CONFIRM:
                    // Emulate mouse click to select an item
                    handleItemClick(selectedButton.item);
                    break;
            }
            if (selectedButtonIndex != previousSelectedButtonIndex) {
                event.preventDefault();
                // Remove the 'active' class from the previously selected button
                selectedButton.element.removeClass('active');
                // Select new button
                selectedButton = selectedMenu.buttons[selectedButtonIndex];
                selectedButton.element.addClass('active');
                // Emulate mouse over when selecting an item
                handleItemMouseOver(selectedMenu, selectedButton.element, selectedButton.item, true);
            }
        });
    }
})();
