/**
 * Created by jvandewync on 8/30/2016.
 */

(function () {
    'use strict';
    angular.module('myApp.dashboard')
        .controller('downloadErrorController', downloadErrorController);

    downloadErrorController.$inject = ['$scope', 'chartGetter', 'FileSaver', 'Blob'];
    function downloadErrorController($scope, chartGetter, FileSaver, Blob) {

        var vm = $scope;

        vm.downloadContextMenu = [
            {text: 'Download', click: downloadCSV}
        ];

        vm.$on('download', function (event, selectedID) {
            console.log(selectedID);
            vm.selectedID = selectedID;
        });

        function downloadCSV() {
            var temp = chartGetter.getCSVbyID(vm.selectedID);
            temp.then(function (chart) {
                var data = new Blob([chart], {type: 'text/plain;charset=utf-8'});
                FileSaver.saveAs(data, vm.selectedID + '.txt');

            }, function (status) {
                console.log(status);
            });
        }
    }
})();
