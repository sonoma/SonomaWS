/**
 * Created by jvandewync on 8/21/2016.
 */

var myApp = angular.module('myApp.dashboard');

myApp.controller('sideMenuContentCtrl',['$scope','$http','$rootScope', 'chartGetter','socketFactory','$controller',function ($scope,$http,$rootScope,chartGetter,socketFactory,$controller) {

    $scope.collapsedOptions = {
        startCollapsed : true
    };

    var temp = chartGetter.grabAllTypedErrors();


    temp.then(function (data) {
        $scope.errorOccurrences = data.data.occurrences;
        $scope.EOOriginalState = {};
        if ($scope.errorOccurrences){
          Object.keys($scope.errorOccurrences).forEach(function (key) {
            $scope.EOOriginalState[key] = 0;
          });
        }
    }, function (status) {
        console.log(status);
    });



    $scope.clickme = function (id) {
        $rootScope.$broadcast('graph', id);
    };

    function keysToLowerCase(obj) {
        if (!typeof(obj) === "object" || typeof(obj) === "string" || typeof(obj) === "number" || typeof(obj) === "boolean") {
            return obj;
        }
        var keys = Object.keys(obj);
        var n = keys.length;
        var lowKey;
        while (n--) {
            var key = keys[n];
            if (key === (lowKey = key.toLowerCase()))
                continue;
            obj[lowKey] = keysToLowerCase(obj[key]);
            delete obj[key];
        }
        return (obj);
    }

    $scope.formatDate = function(obj){
        return moment(obj).format('YYYY-MM-DD HH:mm:ss');
    };

    $scope.getSortedKeys = function(){
        if($scope.errorOccurrences)
            return Object.keys($scope.errorOccurrences).sort();
    };

    socketFactory.init();

    socketFactory.on('newError', function(newError) {

        newError = keysToLowerCase(JSON.parse(newError));
        delete newError.voltagedata;
        delete newError.currentdata;
        newError.beginningdate = $scope.formatDate(newError.beginningdate);

        if($scope.errorOccurrences[newError.categoryname]!==null && $scope.errorOccurrences[newError.categoryname]!=undefined){
            $scope.errorOccurrences[newError.categoryname].unshift(newError);
            $scope.EOOriginalState[newError.categoryname]++;
        }
        else{
            $scope.errorOccurrences[newError.categoryname] = [newError];
            $scope.EOOriginalState[newError.categoryname] = 1;
        }

    });

    angular.extend(this, $controller('downloadErrorController', {$scope: $scope}));

}]);
