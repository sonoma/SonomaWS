/**
 * Created by jvandewync on 8/23/2016.
 */


var myApp = angular.module('myApp.dashboard');


myApp.factory('socketFactory', ['$rootScope', '$window', function ($rootScope, $window) {

    return {
        init: function () {
            //var ioRoom = $window.location.origin + '/' + $window.localStorage.code;
            $window.socket = io(window.location.hostname);
        },

        on: function (eventName, callback) {
            $window.socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply($window.socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            $window.socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply($window.socket, args);
                    }
                });
            });
        }
    }
}]);
