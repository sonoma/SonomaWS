/**
 * Created by jvandewync on 8/21/2016.
 */

var myApp = angular.module('myApp.dashboard');


myApp.factory('chartGetter', ['$http','$q', function ($http,$q) {
    return{
        grabFromDBbyID : function (selectedChartId) {
            var defer = $q.defer();
            $http.get('/secure/chart', {params: {chart_id: selectedChartId}})
                .then( (data) => {
                    defer.resolve(data)
                }
                ,() => {
                });
            return  defer.promise;
        },
        getCSVbyID : function (selectedChartId) {
            var defer = $q.defer();
            $http.get('/secure/download', {params: {chart_id: selectedChartId}})
                .then( (data) => {
                    defer.resolve(data)
                }
                ,() => {
                });
            return  defer.promise;
        },

        grabAllTypedErrors : function () {
            var defer = $q.defer();
            $http.get('/secure/sideMenu/')
                .then( (data) => {
                    defer.resolve(data)
                }
                ,() => {
                });
            return  defer.promise;
        }
    }
}]);
