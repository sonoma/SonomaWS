'use strict';

angular.module('myApp.dashboard', ['ngRoute', 'ngMessages', 'ngAnimate','mgcrea.ngStrap', 'ngFileSaver'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/secure/dashboard/', {
                templateUrl: 'dashboard.html',
                controller : '',
                access: { requiredLogin: true }
            })
    }]);
