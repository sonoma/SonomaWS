(function () {
    'use strict';
    angular.module('myApp', [
        'ngRoute',
        'ngMessages',
        'ngAnimate',
        'myApp.common',
        'myApp.dashboard'
    ]).config(config)

    function config($routeProvider, $locationProvider) {
        $locationProvider.html5Mode({enabled: true});
    }
})();


