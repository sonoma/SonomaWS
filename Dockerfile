FROM node:latest

LABEL authors="Mathieu Tortuyaux <mathieu.tortuyaux@gmail.com>"

RUN apt-get update && apt-get install -y \
	vim \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

RUN npm install -g nodemon bower


WORKDIR /opt/webserver/
COPY . /opt/webserver
RUN npm install && bower install --allow-root


RUN rm -rf /tmp/* /var/tmp/*
