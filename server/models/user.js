/**
 * Created by jvandewync on 8/10/2016.
 */
var mongoose = require('mongoose');
var Email = require('mongoose-type-email');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var SALT_WORK_FACTOR = 10;
var mongooseFindOrCreate = require('mongoose-findorcreate');
var mongooseTimestamp = require('mongoose-timestamp');
var _ = require('lodash');

// Schema
// Note : All fields in a mongoose schema are optional by default (besides _id, of course)
var UserSchema = Schema({
    email: {
        type: Email,
        required: true
    },
    name: {
        first: { type: String, required: true },
        last: { type: String, required: true }
    },
    auth: {
        local: {
            username: {
                type: String,
                index: { unique: true }
            },
            password: {type: String}
        }
    },
    roles: [{ type: String }]
});

// Virtuals
UserSchema.virtual('safeJSON').get(function () {
    return JSON.stringify(this.getSafeJSON());
});
UserSchema.virtual('name.full').get(function () {
    return this.name.first + ' ' + this.name.last;
});
UserSchema.virtual('name.full').set(function (name) {
    this.name.first = name.slice(0, Math.max(1, name.length - 1)).join(' ');
    this.name.last = name.slice(Math.max(1, name.length - 1)).join(' ');
});

// Plugins
UserSchema.plugin(mongooseFindOrCreate);
UserSchema.plugin(mongooseTimestamp);

// Bcrypt middleware
UserSchema.pre('save', function (next) {
    var user = this;

    if (!user.isModified('auth.local.password')) {
        return next();
    }

    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) {
            return next(err);
        }
        bcrypt.hash(user.auth.local.password, salt, null, function (err, hash) {
            if (err) {
                return next(err);
            }
            user.auth.local.password = hash;
            next(null, hash);
        });
    });
});

// Promote user to admin if admin does not yet exist.
UserSchema.pre('save', function (next) {
    var user = this;
    getUserModel().count({ roles: 'admin' }, function (err, count) {
        if (err) {
            return next(err);
        }
        if (count === 0) {
            user.roles.push('admin');
        }
        next();
    });
});

// Password verification
UserSchema.methods.comparePassword = function (candidatePassword, cb) {
    var user = this;
    bcrypt.compare(candidatePassword, user.auth.local.password, cb);
};

// Safe JSON (internal data removed)
UserSchema.methods.getSafeJSON = function () {
    var user = this.toJSON();

    user.id = user._id;
    delete user._id;
    delete user.__v;

    if (user.auth.local) {
        delete user.auth.local.password;
    }

    return user;
};

var getUserModel = function () {
    return mongoose.model('User', UserSchema);
};

UserSchema.statics.findOrCreateLocal = function (profile, cb) {
    var data = {
        email: profile.email,
        name: {
            first: profile.firstName,
            last: profile.lastName
        },
        'auth.local': {
            username: profile.username,
            password: profile.password
        }
    };
    getUserModel().findOne({'auth.local.username': profile.username}, function (err, user) {
        if (err) { return cb(err); }
        if (user) {
            if (_.isEmpty(user.get('auth.local'))) {
                user.auth.local.username = profile.username;
                user.auth.local.password = profile.password;
                getUserModel().save(cb);
            } else {
                user.comparePassword(profile.password, function (err, matched) {
                    if (err) {
                        return cb(err);
                    }
                    if (matched) {
                        // Update existing account.
                        user.name = data.name;
                        user.save(cb);
                    } else {
                        // Account created by someone else or using non-local.
                        return cb(new Error('Account already exists.'));
                    }
                });
            }
        } else {
            // Create new account.
            getUserModel().create(data, cb);
        }
    });
};

module.exports = mongoose.model('User', UserSchema);