/**
 * Created by jvandewync on 8/19/2016.
 */

var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;

var FREQUENCY = 10000; //10 kHz
var PERIOD = 1 / FREQUENCY;

var ErrorDataSchema = Schema({
    categoryname: String,
    beginningdate: Date,
    voltagedata: [Number],
    currentdata: [Number]
});


var getErrorDataModel = function () {
    return mongoose.model('ErrorData', ErrorDataSchema, "data");
};

ErrorDataSchema.statics.findAllOccurrences = function (cb) {
    getErrorDataModel().find({}).sort({categoryname: 1, beginningdate: -1}).select({
        categoryname: 1,
        beginningdate: 1
    }).exec(function (err, occurrences) {
        if (err) {
            return cb(err);
        }
        if (null != occurrences) {

            var resultJson = JSON.parse(JSON.stringify(occurrences));
            var hashmap = {};

            resultJson.forEach(function (element) {
                if (hashmap[element.categoryname] !== null && hashmap[element.categoryname] != undefined) {
                    hashmap[element.categoryname].push(element);
                }
                else {
                    hashmap[element.categoryname] = [element];
                }
            });

            return cb(null, hashmap);
        }
    });
};

ErrorDataSchema.statics.findById = function (chartId, cb) {
    getErrorDataModel().findOne({'_id': chartId}, function (err, chart) {
        if (err) {
            return cb(err);
        }
        if (null != chart) {
            //console.log(chart.xAxis);
            var resultJson = JSON.parse(JSON.stringify(chart));

            var baseDate = moment(resultJson.beginningdate);
            resultJson.timeAxis = [baseDate.clone().format('YYYY-MM-DD HH:mm:ss.SSSSSS')];
            resultJson.voltagedata[0] = resultJson.voltagedata[0].toFixed(6);
            resultJson.currentdata[0] = resultJson.currentdata[0].toFixed(6);
            var t = 0;
            for (i = 1; i <  resultJson.voltagedata.length; i++) {
                t += PERIOD;
                if (t >= 0.001) {
                    resultJson.timeAxis.push(baseDate.add(1, 'milliseconds').format('YYYY-MM-DD HH:mm:ss.SSSSSS'));
                    t = 0;
                }
                else {
                    resultJson.timeAxis.push(baseDate.format('YYYY-MM-DD HH:mm:ss.SSS') + t * 1000000);
                }

                resultJson.voltagedata[i] = resultJson.voltagedata[i].toFixed(6);
                resultJson.currentdata[i] = resultJson.currentdata[i].toFixed(6);
            }

            resultJson.errorMarkLine = FREQUENCY+1;

            return cb(null, resultJson);
        }
    });
};

ErrorDataSchema.statics.convert2CSV = function (chartId, cb) {
    getErrorDataModel().findOne({'_id': chartId}, function (err, chart) {
        if (err) {
            return cb(err);
        }
        if (null != chart) {
            //console.log(chart.xAxis);
            var resultJson = JSON.parse(JSON.stringify(chart));

            var csv = {};
            csv.fields = ['time', 'voltage', 'current'];
            csv.data = [];
            var baseDate = moment(resultJson.beginningdate);
            var processedTime = baseDate.clone().format('YYYY-MM-DD HH:mm:ss.SSSSSS');
            var t = 0;

            csv.data.push(
                {
                    "time": processedTime,
                    "voltage": resultJson.voltagedata[0].toFixed(6),
                    "current": resultJson.voltagedata[0].toFixed(6)
                });
            for (var i = 1; i < resultJson.voltagedata.length; i++) {
                t += PERIOD;

                if (t >= 0.001) {
                    processedTime = baseDate.add(1, 'milliseconds').format('YYYY-MM-DD HH:mm:ss.SSSSSS');
                    t = 0;
                }
                else {
                    processedTime = baseDate.format('YYYY-MM-DD HH:mm:ss.SSS') + t * 1000000;
                }

                csv.data.push(
                    {
                        "time": processedTime,
                        "voltage": resultJson.voltagedata[i].toFixed(6),
                        "current": resultJson.voltagedata[i].toFixed(6)
                    });
            }

            return cb(null, csv);
        }
    });
};

module.exports = mongoose.model('ErrorData', ErrorDataSchema, "data");
