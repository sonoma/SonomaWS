/**
 * Created by jvandewync on 8/10/2016.
 */

var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');

module.exports = function (passport) {

    //Passport session setup.
    //   To support persistent login sessions, Passport needs to be able to
    //   serialize users into and deserialize users out of the session.  Typically,
    //   this will be as simple as storing the user ID when serializing, and finding
    //   the user by ID when deserializing.
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // deserializeUser is passed a function that will return the user the
    // belongs to an id
    passport.deserializeUser(function (id, done) {
        User.findOne({_id: id}, function (err, user) {
            done(err, user);
        });
    });

    // Use the LocalStrategy within Passport.
    //   Strategies in passport require a `verify` function, which accept
    //   credentials (in this case, a username and password), and invoke a callback
    //   with a user object.

    passport.use('local-login',new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true
    },function (req,username, password, done) {
        //login with either Email or Username
        process.nextTick(function () {
            var criteria = (username.indexOf('@') === -1) ? {'auth.local.username': username} : {'email': username};
            User.findOne(criteria, function (err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, {message: 'Unknown user '+ username});
                }
                user.comparePassword(password, function (err, matched) {
                    if (err) {
                        return done(err);
                    }
                    if (matched) {
                        return done(null, user);
                    } else {
                        return done(null, false, {message: 'Invalid password'});
                    }
                });
            });
        });
    }));
};