
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = 3000;

var path = require('path');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var morgan = require('morgan');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var passport = require('passport');
var flash = require('connect-flash');
var MongoStore = require('connect-mongo')(session);

var configDB = require('./config/database.js');

var nats = require('nats').connect("nats://nats_container:4222");
nats.on('error', function(e) {
    console.log('Error [' + nats.options.url + ']: ' + e);
    process.exit();
});

// TODO : https://github.com/Automattic/mongoose/issues/4291
// mpromise (mongoose's default promise library) is deprecated
mongoose.Promise = global.Promise;
mongoose.connect(configDB.url);
require('./config/passport')(passport);

app.use(express.static(path.join(__dirname, "../client")));
app.use(express.static(path.join(__dirname, "../client/app/dashboard")));
app.use(express.static(path.join(__dirname, "../client/app/common")));


app.use(morgan('dev'));

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


//TODO : https://github.com/jdesboeufs/connect-mongo#create-a-new-connection-from-a-mongodb-connection-string
app.use(session({secret: 'anystringoftext',
    saveUninitialized: true,
    resave: true,
    store: new MongoStore({ mongooseConnection: mongoose.connection,
        ttl: 2 * 24 * 60 * 60 })})); //two days

app.use(flash()); // use connect-flash for flash messages stored in session

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

server.listen(port);
io.on('connection', function(socket){

    // Simple Subscriber
    nats.subscribe('newError', function(data) {
        socket.emit('newError', data);
        console.log("New client");
    });

    nats.on('close', function() {
        console.log('CLOSED');
        process.exit();
    });
});

var auth = express.Router();
require('./routes/auth.js')(auth, passport);
app.use('/auth', auth);


var secure = express.Router();
require('./routes/secure.js')(secure);
app.use('/secure', secure);


app.route('*').get(function(req, res) {
    return res.sendFile(path.join(path.join(__dirname, "../client"), 'index.html'));
});


app.get('*', function(req, res) {
    res.redirect('/');
});


app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.send(500, { message: err.message });
});


app.get('*', function(req, res){
    return res.status(404).redirect('/#pageNotFound');
    //res.redirect('/server-error');
});

//app.listen(port);
console.log('Server running on port: ' + port);
