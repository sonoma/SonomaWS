var ErrorData = require('../models/errorData');
var json2csv = require('json2csv');

module.exports = function (router) {

    router.use(function (req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }
        res.redirect('/#auth/login');
    });

    router.get('/dashboard', function (req, res) {
        res.redirect('/#secure/dashboard');
    });

    router.get('/sideMenu', function (req, res) {
        ErrorData.findAllOccurrences(function (err, occurrences) {
            if (occurrences) {
                res.json({success: true, occurrences: occurrences});
            }
        })
    });


    router.get('/chart', function (req, res) {
        var chart_id = req.query.chart_id;
        if (null != chart_id) {
            ErrorData.findById(chart_id, function (err, data) {
                if (data) {
                    res.json({success: true, selectedChart: data});
                }
            })
        }
    });

    router.get('/download', function (req, res) {
        var chart_id = req.query.chart_id;
        if (null != chart_id) {
            ErrorData.convert2CSV(chart_id, function (err, csvJson) {
                if (csvJson) {
                    json2csv({data: csvJson.data, fields: csvJson.fields}, function (err, csv) {
                        if (err) {
                            console.log(err);
                        }
                        res.set({
                            'Content-Disposition': 'attachment; filename=test.csv',
                            'Content-Type': 'text/csv'
                        });
                        res.send(csv);
                    });
                }
            })
        }
    });
};