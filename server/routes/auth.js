/**
 * Created by jvandewync on 8/10/2016.
 */

var User = require('../models/user');
var _ = require('lodash');
var jwt = require('jsonwebtoken');

const fs = require('fs');
var path = require('path');
var cert = fs.readFileSync(path.join(__dirname, '../config/private.key'));  // get private key generated using https://www.grc.com/passwords.htm

var validator = require('validator');
var MIN_PASSWORD_LENGTH = 6;
var MAX_PASSWORD_LENGTH = 20;

module.exports = function (router, passport) {

    //localhost:8080/auth/login
    router.post('/login', function (req, res) {

        var username = req.body.username || req.body.email;
        var password = req.body.password;

        if (password === undefined || username === undefined) {
            return res.status(400);
        }

        // Validations
        //username or email
        if (_.isEmpty(username)) {
            return res.status(422).send({message: 'Email or username is empty'})
        }
        else if (!validator.isEmail(username) && !validator.isAlphanumeric(username)) {
            return res.status(422).send({message: 'The email or password is incorrect.', username: username});
        }

        //password
        if (_.isEmpty(password)) {
            return res.status(422).send({message: 'The email or password is incorrect.'});
        }
        else if (/ /.test(password) || password.length < MIN_PASSWORD_LENGTH || password.length > MAX_PASSWORD_LENGTH) {
            return res.status(422).send({message: 'The email or password is incorrect.'});
        }

        passport.authenticate('local-login', function (err, user, info) {
            if (err) {
                return res.status(500).send({message: err});
            }
            else if (!user) {
                return res.status(422).send({message: 'The email or password is incorrect.'});
            }
            else {
                req.logIn(user, function (err) {
                    if (err) {
                        return res.send(500, {message: err});
                    }
                    var token = jwt.sign(user.getSafeJSON(), cert, {
                        algorithm: 'RS256',
                        expiresIn: "2 days"
                    });
                    res.json({success: true, token: token});
                });
            }
        })(req, req.res, req.next);
    });

    //localhost:8080/auth/signup
    router.post('/signup', function (req, res) {

        if (req.body.username === undefined || req.body.email === undefined || req.body.password === undefined || req.body.firstName === undefined || req.body.lastName === undefined) {
            return res.status(400);
        }

        var errMsgs = {};
        // Validations
        // username
        if (_.isEmpty(req.body.username)) {
            errMsgs.username = 'Username is required.';
        } else if (!validator.isAlphanumeric(req.body.username)) {
            errMsgs.username = 'Username must be made of only lowercase letters and numbers';
        }

        // email
        if (!validator.isEmail(req.body.email)) {
            errMsgs.email = 'Email must be a valid e-mail.';
        }
        //password
        if (_.isEmpty(req.body.password)) {
            errMsgs.password = 'Password is required.';
        } else if (/ /.test(req.body.password)) {
            errMsgs.password = 'Password cannot have spaces.';
        } else if (req.body.password.length < MIN_PASSWORD_LENGTH || req.body.password.length > MAX_PASSWORD_LENGTH) {
            errMsgs.password = 'Password must have at least 6 and at most 20 characters.';
        }
        //first name
        if (_.isEmpty(req.body.firstName)) {
            errMsgs.firstName = 'First name is required.';
        } else if (!validator.isAlpha(req.body.firstName)) {
            errMsgs.firstName = 'First name must made of only letters.';
        }

        //last name
        if (_.isEmpty(req.body.lastName)) {
            errMsgs.lastName = 'Last name is required.';
        } else if (!validator.isAlpha(req.body.lastName)) {
            errMsgs.lastName = 'Last name must made of only letters.';
        }

        if (_.size(errMsgs)) {
            return res.status(422).send({error: errMsgs});
        }

        User.findOrCreateLocal(req.body, function (err, user) {
            if (err) {
                return res.status(422).send({message: err.message});
            }
            req.logIn(user, function (err) {
                if (err) {
                    return res.status(500).send({message: err.message});
                }
                var token = jwt.sign(user.getSafeJSON(), cert, {
                    algorithm: 'RS256',
                    expiresIn: "2 days"
                });
                res.json({success: true, token: token});
            });
        });
    });

    router.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    router.get('/login', function (req, res) {
        res.redirect('/#auth/login');
    });

    router.get('/signup', function (req, res) {
        res.redirect('/#auth/signup');
    });
};